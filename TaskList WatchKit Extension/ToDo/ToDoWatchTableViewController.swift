//
//  ToDoWatchTableViewController.swift
//  TaskList WatchKit Extension
//
//  Created by parrot on 2018-12-04.
//  Copyright © 2018 room1. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class ToDoWatchTableViewController: WKInterfaceController, WCSessionDelegate {

    // 1. Table data source
    var items:[String] = []
    
    // 2. outlets
    @IBOutlet var tableView: WKInterfaceTable!
    
    // 3. Make a session variable
    let session = WCSession.default
    
    // 4. mandatory functions
    override func awake(withContext context: Any?) {
        
        
        // put items into tableview
        tableView.setNumberOfRows(items.count, withRowType: "ToDoRowControllerIdentifier")
        
        for i in 0..<items.count {
            let row = tableView.rowController(at: i) as! ToDoRowController
            row.taskLabel.setText(items[i])
        }
    }
    
    //  Custom function to deal with data sent from the iPhone
    func getDataFromPhone() {
        
        // get items from the iPhone
        let iPhoneData = session.receivedApplicationContext as? [String: Any]
        
        if (iPhoneData != nil) {
            items = iPhoneData!["tasks"]! as! [String]
        }
    }
    
    
    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
        //You should never update UI in iOS or watchOS from any thread other than the Main Thread.
        DispatchQueue.main.async() {
            // Get the data from the iPhone
            self.getDataFromPhone()
        }
    }
    
    // MARK: WCSessionDelegate functions
    // -------------
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {}
}
