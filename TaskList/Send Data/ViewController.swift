//
//  ViewController.swift
//  TaskList
//
//  Created by parrot on 2018-12-04.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit
import WatchConnectivity        // 0. import WatchConnectivity library

// 0. Include the WCSessionDelegate variables
class ViewController: UIViewController,WCSessionDelegate  {

    
    // 1. Create a session variable to act as a connection betweeen phone and watch
    var session: WCSession?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // 2. Check if the device supports WCSession
        // Note: WCSession NOT Supported on iPads!
        // So if person is using an IPad, do not connection
        if WCSession.isSupported() {
            // This code only gets run if person is using an iPhone
            // - Activate the session between the phone and watch!
            session = WCSession.default
            session?.delegate = self
            session?.activate()
            
            if session?.isPaired != true {
                print("Apple Watch is not paired")
            }
            
            if session?.isWatchAppInstalled != true {
                print("WatchKit app is not installed")
            }
        }
        else {
            print("Are you using an iPad? WatchConnectivity is not supported on this device. ")
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: Changed switch
    @IBAction func switchChanged(_ sender: UISwitch) {
        print("Switch changed!")
        if (session != nil) {
            print("Trying to send data to watch!")
            // 3. This is the data we are sending to the watch
            // Use a dictionary to send more than 1 piece of data
            // Otherwise you can just use a regular variable
            let dataToSend:[String:Any] = ["switchPosition": sender.isOn]
            
            do {
                
                // 4. try to open a connection to the watch and send the data over
                try session?.updateApplicationContext(dataToSend)
            }
            catch {
                print("error")
            }
        }
        
    }
    
    // MARK:  Watch Connectivity Functions (required)
    // -------------------
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) { }
    func sessionDidBecomeInactive(_ session: WCSession) { }
    func sessionDidDeactivate(_ session: WCSession) { }
    
    
    

    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
