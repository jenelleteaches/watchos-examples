//
//  InterfaceController.swift
//  TaskList WatchKit Extension
//
//  Created by parrot on 2018-12-03.
//  Copyright © 2018 room1. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {

    // MARK: Outlets
    // -------------
    @IBOutlet var messageLabel: WKInterfaceLabel!
    
    // MARK: Session variables (connect with iPhone)
    // -------------
    let session = WCSession.default
    
    // MARK: WCSessionDelegate functions
    // -------------
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {}
    
    // MARK: Default functions
    // -------------
    // equivalent to the viewDidLoad function
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // App is loaded and we want it to use that last received value,
        // but there wasn’t a new one while it was closed.
        // We call our helper method early in our view’s lifecycle to set that label,
        // so our awakeWithContext now looks like:
        self.getDataFromPhone()
        
        
        // Do NOT need to do the same session checks as the iPhone
        // because a WatchOS will always support WCSession
        session.delegate = self
        session.activate()
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    //  Custom function to deal with data sent from the iPhone
    func getDataFromPhone() {
        // receivedApplicationContext --> function to get data from iPhone
        let iPhoneData = session.receivedApplicationContext as? [String: Bool]
        
        if (iPhoneData != nil) {
            if iPhoneData!["switchPosition"] == true {
                print("switch is on")
                self.messageLabel.setText("ON!")
            }
            else {
                print("switch off")
                self.messageLabel.setText("OFF!")
            }
        }
        
    }
    
    
    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
        //You should never update UI in iOS or watchOS from any thread other than the Main Thread.
        DispatchQueue.main.async() {
            // Get the data from the iPhone
            self.getDataFromPhone()
        }
    }
    
    
    
    
}
